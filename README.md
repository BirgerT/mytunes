# MyTunes 🎵
The seventh assignment for the Experis Academy Noroff upskill course.

A REST API that provides access to a database of music and customer related data.
All of the features provided are described in the features section below.

## Setup
<ul>
<li> Clone the repository </li>
<li> Open it in Visual Studio </li>
<li> Set the DATABASE_SOURCE to your DB server name in launchSettings.json </li>
</ul>

## Run
<ul>
<li> Run the app in Visual Studio </li>
<li> Open Postman </li>
<li> Import the Postman collection from the Postman Collection folder </li>
<li> Send the requests </li>
</ul>

## Features

The following endpoints are provided with this API:

**Base url:** https://localhost:44332/api

### GET: /music
Returns a json object with lists of 5 random artists, 5 random tracks and 5 random genres.

### GET: /music/search?query={query}
Returns a list of search results where the track name, artist name or album title contains the query string.
The result contains the track ID, track name, artist name, album title and the genre.

### GET: /music/search/track?track={query}
Returns a list of search results where the track name contains the query string.
The result contains the track ID, track name, artist name, album title and the genre.

### GET: /customers
Returns a list of all the customers.
Each customer has a first and last name, country, postal code, phone number and email address.

### POST: /customers
Adds a new customer to the database.
If successful returns the newly created customer.

### PUT: /customers/id
Update an existing customer.
If successful returns the updated customer.

### GET: /customers/countries
Returns the number of customers for each country, in descending order.

### GET: /customers/invoices
Returns a list of the top spending customers, in descending order.

### GET: /{id}/popular/genre
Returns the most popular genre of the customer with the given id.
Most popular in this context means the genre that corresponds to the most tracks from invoices associated to that customer.

## Authors
<ul>
<li> Birger Topphol </li>
<li> Camilla Arntzen </li>
</ul>

## Assignment requirements

### Music
<p> &#x2611; 1. Get 5 random artists, 5 random songs and 5 random genres. </p>
<p> &#x2611; 2. Search for a song with match on song title. </p>
<p> &#x2611; Extra. Search with match on artist and album as well. </p>


### Customers
<p> &#x2611; 1. Read all the customers in the db. </p>
<p> &#x2611; 2. Add a new customer to the db. </p>
<p> &#x2611; 3. Update an existing customer. </p>
<p> &#x2611; 4. Return the number of customers in each country, ordered descending. </p>
<p> &#x2611; 5. Get customers who are the highest spenders. </p>
<p> &#x2611; 6. Get most popular genre for a given customer. </p>

