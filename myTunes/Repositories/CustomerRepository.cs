﻿using myTunes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Repositories
{
    public class CustomerRepository
    {

        //Get all customers
        public List<Customer> GetCustomers()
        {
          
            List<Customer> customerList = new List<Customer>(); //List of customers

            //SQL-Query
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
            
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    
                    using SqlDataReader reader = cmd.ExecuteReader();
                 
                    while (reader.Read())
                    {
                        
                        Customer customer = new Customer();
                        customer.CustomerId = reader.GetInt32(0);
                        customer.FirstName = reader.GetString(1);
                        customer.LastName = reader.GetString(2);
                        customer.Country = reader.GetString(3);
                        customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                        customer.Phone = reader.IsDBNull(4) ? null : reader.GetString(4);
                        customer.Email = reader.GetString(6);
                        customerList.Add(customer);
                    }
                }
            }
            catch (SqlException ex) //catch exceptions
            {
                Console.WriteLine(ex);
            }

            return customerList; //returns the list of customers
        }

        //Add a new customer:
        public bool AddNewCustomer(Customer customer)
        {

            if (customer == null) return false;
            bool success = false;

            //SQL-query
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                cmd.Parameters.AddWithValue("@Country", customer.Country);
                cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                cmd.Parameters.AddWithValue("@Email", customer.Email);
                success = cmd.ExecuteNonQuery() > 0;
            }
            catch (SqlException ex) //catch exceptions
            {
                Console.WriteLine(ex);
            }
            return success;

        }

        //Update a customer
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            
            //SQL-query
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName," +
                " Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email" +
                " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
               
            }
            return success;


        }

        //Get customers by country
        public List<CountryCustomer> GetNumCustomersInCountry()
        {
            List<CountryCustomer> customerCountryList = new List<CountryCustomer>(); //List of countries and number of customers

            //SQL-query
            string sql = "SELECT Customer.Country, COUNT(Customer.Country) AS Customers FROM Customer " +
                         "GROUP BY Country " +
                         "ORDER BY Customers DESC";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                
                using SqlCommand cmd = new SqlCommand(sql, conn);
             
                using SqlDataReader reader = cmd.ExecuteReader();
               
                while (reader.Read())
                {
                    
                    CountryCustomer countryCustomer = new CountryCustomer
                    {
                        Country = reader.GetString(0),
                        Customers = reader.GetInt32(1),
                    };
                    customerCountryList.Add(countryCustomer);
                }
            }
            
            catch (SqlException ex) //catch exception
            {
                Console.WriteLine(ex);
                return null;
            }

            return customerCountryList; //returns the list

        }


        //Fetch highest spenders
        public List <CustomerSpending> GetHighestSpendingCustomers()
        {
            List<CustomerSpending> customerSpendings = new List<CustomerSpending>();

            //SQL-query
            string sql = "SELECT Customer.CustomerId, FirstName, LastName, SUM(Total) AS Total FROM Invoice, Customer " +
                         "WHERE Customer.CustomerId = Invoice.CustomerId " +
                         "GROUP BY Customer.CustomerId, FirstName, LastName " +
                         "ORDER BY Total DESC;";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
              
                using SqlCommand cmd = new SqlCommand(sql, conn);
               
                using SqlDataReader reader = cmd.ExecuteReader();
               
                while (reader.Read())
                {
                   
                    CustomerSpending customerSpending = new CustomerSpending
                    {
                        CustomerId = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        TotalAmount = reader.GetDecimal(3)
                    };
                    customerSpendings.Add(customerSpending);
                }
            }
            catch (SqlException ex)//catch exception
            {
                Console.WriteLine(ex);
            }

            return customerSpendings;

        }

        //6. Get most popular genre for given customer
        public List<CustomerGenre> GetMostPopularGenre(int id) 
        {
            List<CustomerGenre> popularGenre = new List<CustomerGenre>(); //List with genre name and invoices for the genre

            string sql = "SELECT Genre.Name, COUNT(Genre.Name) " +
                            "FROM Customer " +
                            "INNER JOIN Invoice on Invoice.CustomerId = @CustomerId " +
                            "INNER JOIN InvoiceLine on InvoiceLine.InvoiceId = Invoice.InvoiceId, " +
                            "Genre, " +
                            "Track " +
                            "WHERE Invoice.CustomerId = Customer.CustomerId " +
                            "AND InvoiceLine.TrackId = Track.TrackId " +
                            "AND Track.GenreId = Genre.GenreId " +
                            "GROUP BY Genre.Name " +
                            "ORDER BY COUNT(Genre.Name) desc";
;

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CustomerId", id);
                using SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (popularGenre.Any())
                    {
                        if (reader.GetInt32(1) < popularGenre[0].InvoicesForGenre)
                            break;
                    }
                    CustomerGenre topGenre = new CustomerGenre()
                    {
                        Genre = reader.GetString(0),
                        InvoicesForGenre = reader.GetInt32(1)
                    };
                    popularGenre.Add(topGenre);
                }
            }
            catch (SqlException ex) //catch exceptions
            {
                Console.WriteLine(ex);
                return null;
            }
            Console.WriteLine(popularGenre);

            return popularGenre; //returns the list
        }

    }
}
