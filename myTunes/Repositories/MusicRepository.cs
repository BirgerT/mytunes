﻿using myTunes.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace myTunes.Repositories
{
    public class MusicRepository
    {
        #region Public API
        // The main functions called by the controller.
        // These methods utilizes a couple of helper functions to execute the queries and extract the data.

        // Returns a list of N randomly selected artists from the DB.
        public List<Artist> FetchRandomArtists(int count)
        {
            // SQL query that selects tho top N artists based on a random ordering.
            string sql = $"SELECT TOP {count} ArtistId, Name FROM Artist ORDER BY NEWID()";

            DataTable result = ExecuteSql(sql);
            return ExtractArtists(result);
        }

        // Returns a list of randomly selected tracks from the DB.
        public List<Track> FetchRandomTracks(int count)
        {
            // SQL query that selects tho top N tracks based on a random ordering.
            string sql = $"SELECT TOP {count} TrackId, Name FROM Track ORDER BY NEWID()";
            
            DataTable result = ExecuteSql(sql);
            return ExtractTracks(result);
        }

        // Returns a list of randomly selected genres from the DB.
        public List<Genre> FetchRandomGenres(int count)
        {
            // SQL query that selects tho top N genres based on a random ordering.
            string sql = $"SELECT TOP {count} GenreId, Name FROM Genre ORDER BY NEWID()";

            DataTable result = ExecuteSql(sql);
            return ExtractGenres(result);
        }

        // Searches for tracks, and returns a list if any of their names match (partial match included).
        public List<SearchResult> SearchForTrack(string query)
        {
            // SQL query that looks for track with name that matches the query, and retrieves artist name, album title and genre from other tables.
            string sql = 
                $"SELECT TrackId, Track.Name, Artist.Name, Album.Title, Genre.Name " +
                $"FROM Track " +
                $"JOIN Album ON Track.AlbumId = Album.AlbumId " +
                $"JOIN Artist ON Album.ArtistId = Artist.ArtistId " +
                $"JOIN Genre ON Track.GenreId = Genre.GenreId " +
                $"WHERE Track.Name LIKE '%{query}%'";

            DataTable result = ExecuteSql(sql);
            return ExtractSearchResults(result);
        }

        // Searches for tracks, and returns a list if any of their names, artist names or album titles match (partial match included).
        public List<SearchResult> Search(string query)
        {
            // SQL query that looks for track with name that matches the query, and retrieves artist name, album title and genre from other tables.
            string sql = 
                $"SELECT TrackId, Track.Name, Artist.Name, Album.Title, Genre.Name " +
                $"FROM Track " +
                $"JOIN Album ON Track.AlbumId = Album.AlbumId " +
                $"JOIN Artist ON Album.ArtistId = Artist.ArtistId " +
                $"JOIN Genre ON Track.GenreId = Genre.GenreId " +
                $"WHERE Track.Name LIKE '%{query}%' " +
                $"OR Album.Title LIKE '%{query}%' " +
                $"OR Artist.Name LIKE '%{query}%'";

            DataTable result = ExecuteSql(sql);
            return ExtractSearchResults(result);
        }

        #endregion

        #region Helper functions

        // Executes an SQL query and returns a table with the fetched data.
        private static DataTable ExecuteSql(string sql)
        {
            DataTable dataTable = new DataTable();

            try
            {
                // Open a connection to the DB, using a helper class.
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();

                // Execute the SQL query.
                using SqlCommand cmd = new SqlCommand(sql, conn);
                using SqlDataReader reader = cmd.ExecuteReader();

                // Load the data into the table.
                dataTable.Load(reader);
            }
            catch (SqlException ex)
            {
                // Log to console.
                throw;
            }

            return dataTable;
        }


        // Extracts a list of artists from a data table.
        private static List<Artist> ExtractArtists(DataTable result)
        {
            List<Artist> artists = new List<Artist>();

            // Iterate through each row.
            foreach (DataRow row in result.Rows)
            {
                // Create and add a new artist based on the data in the row.
                artists.Add(new Artist
                {
                    ArtistId = (int)row.ItemArray[0],
                    Name = (string)row.ItemArray[1]
                });
            }
            return artists;
        }

        // Extracts a list of tracks from a data table.
        private static List<Track> ExtractTracks(DataTable result)
        {
            List<Track> tracks = new List<Track>();

            // Iterate through each row.
            foreach (DataRow row in result.Rows)
            {
                // Create and add a new track based on the data in the row.
                tracks.Add(new Track
                {
                    TrackId = (int)row.ItemArray[0],
                    Name = (string)row.ItemArray[1]
                });
            }
            return tracks;
        }

        // Extracts a list of genres from a data table.
        private static List<Genre> ExtractGenres(DataTable result)
        {
            List<Genre> genres = new List<Genre>();

            // Iterate through each row.
            foreach (DataRow row in result.Rows)
            {
                // Create and add a new genre based on the data in the row.
                genres.Add(new Genre
                {
                    GenreId = (int)row.ItemArray[0],
                    Name = (string)row.ItemArray[1]
                });
            }
            return genres;
        }

        // Extracts a list of search results from a data table.
        private static List<SearchResult> ExtractSearchResults(DataTable result)
        {
            List<SearchResult> searchResult = new List<SearchResult>();

            // Iterate through each row.
            foreach (DataRow row in result.Rows)
            {
                // Create and add a new search result based on the data in the row.
                searchResult.Add(new SearchResult
                {
                    TrackId = (int)row.ItemArray[0],
                    TrackName = (string)row.ItemArray[1],
                    ArtistName = (string)row.ItemArray[2],
                    AlbumTitle = (string)row.ItemArray[3],
                    Genre = (string)row.ItemArray[4]
                });
            }
            return searchResult;
        }

        #endregion
    }
}
