﻿using System;
using System.Data.SqlClient;

namespace myTunes.Repositories
{
    public class ConnectionHelper
    {
        public static string GetConnectionstring()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = Environment.GetEnvironmentVariable("DATABASE_SOURCE"); //Gets data source from environment variable in launchSettings.json
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            return connectionStringBuilder.ConnectionString;
        }
    }
}
