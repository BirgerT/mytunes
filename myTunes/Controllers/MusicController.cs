﻿using Microsoft.AspNetCore.Mvc;
using myTunes.Models;
using myTunes.Repositories;
using System.Collections.Generic;

namespace myTunes.Controllers
{
    [Route("api/music")]
    [ApiController]
    public class MusicController : ControllerBase
    {
        private readonly MusicRepository _musicRepository;

        // Uses dependency injection on music repository.
        public MusicController(MusicRepository musicRepository)
        {
            _musicRepository = musicRepository;
        }

        // GET: api/music
        [HttpGet]
        public ActionResult<IEnumerable<object>> Get()
        {
            // Fetch 5 random artists, tracks and genres.
            List<Artist> artists = _musicRepository.FetchRandomArtists(5);
            List<Track> tracks = _musicRepository.FetchRandomTracks(5);
            List<Genre> genres = _musicRepository.FetchRandomGenres(5);

            // Return a json object containing all the lists.
            return Ok(new { artists, tracks, genres });
        }

        // GET: api/music/search?query=foo
        [HttpGet("search")]
        public ActionResult<IEnumerable<SearchResult>> Search(string query)
        {
            // Search for tracks where track name, artist name or album title matching the given query.
            List<SearchResult> results = _musicRepository.Search(query);

            return Ok(results);
        }

        // GET: api/music/search/track?track=foo
        [HttpGet("search/track")]
        public ActionResult<IEnumerable<SearchResult>> SearchTrack(string track)
        {
            // Search for tracks matching the given query.
            List<SearchResult> results = _musicRepository.SearchForTrack(track);

            return Ok(results);
        }
    }
}
