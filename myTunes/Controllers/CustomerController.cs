﻿using Microsoft.AspNetCore.Mvc;
using myTunes.Models;
using myTunes.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Controllers
{
    //Customer controller
    [ApiController]
    ///Set the route of the controller to "api/customers". The other methods in the controller will extend this route. 
    [Route("api/customers")]
    public class CustomerController : ControllerBase
    {

        private readonly CustomerRepository _customerRepository;

        // Uses dependency injection on music repository.
        public CustomerController(CustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        //1) Read all customers in the db
        // GET: api/customers
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            //Calls the method in the repository class that fetches all the customers
            return Ok(_customerRepository.GetCustomers());
        }

        //2) Add a new customer to the db
        // POST: api/customers
        [HttpPost]
        public ActionResult<string> Post(Customer customer)
        {
            //Add  a new Customer 
            bool success = _customerRepository.AddNewCustomer(customer);
            if (!success)
                return StatusCode(500);
            return NoContent();
        }

        //3) Update an existing customer
        // PUT: api/customers/id
        [HttpPut]
        public ActionResult<string> PutCustomer(Customer customer)
        {
            bool success = _customerRepository.UpdateCustomer(customer);
            if (!success)
                return StatusCode(500);
            return NoContent();
        }

        //4) Return the number of customers in each country, ordered descending
        // GET: api/customers
        [HttpGet("countries")]
        public ActionResult<IEnumerable<CountryCustomer>> GetCustomersByCountry()
        {
            //Calls the method in the repository class that gets number of customers in each country
            return Ok(_customerRepository.GetNumCustomersInCountry());
        }

        //5) Customers who are the highest spenders, ordered descending
        //GET: api/customers/invoices
        [HttpGet("invoices")]
        public ActionResult<IEnumerable<CustomerSpending>> HighestSpendingCustomers()
        {
            //Calls the method in the repository class that fetches the highest spending customers
            return Ok(_customerRepository.GetHighestSpendingCustomers());
        }

        //6) For a given customer, their most popular genre (in the case of a tie, display both). 
        //Most popular in this context means the genre that corresponds to the most tracks from invoices associated to that customer.
        //GET: api/customers/id/popular/genre
        [HttpGet("{id:int}/popular/genre")]
        public ActionResult<IEnumerable<CustomerGenre>> CustomerMostPopularGenre( int id)
        {
            //Calls the method in the repository class that gets the most popular genre for a given customer
            return Ok(_customerRepository.GetMostPopularGenre(id));
            
        }
    }
}
