﻿namespace myTunes.Models
{
    public class Genre
    {
        //Id of genre
        public int GenreId { get; set; }

        //Name of genre
        public string Name { get; set; }
    }
}
