﻿namespace myTunes.Models
{
    public class Track
    {
        public int TrackId { get; set; }
        public string Name { get; set; }
    }
}
