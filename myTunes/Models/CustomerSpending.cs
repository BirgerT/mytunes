﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class CustomerSpending
    {
        //Id of customer
        public int CustomerId { get; set; }

        //First name of customer
        public string FirstName { get; set; }

        //Last name of customer
        public string LastName { get; set; }

        //Customer's total amount spent
        public decimal TotalAmount { get; set; }

    }
}
