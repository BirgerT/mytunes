﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class CountryCustomer
    {
        //Name of the country
        public string Country { get; set; }

        //Number of customers in the country
        public int Customers { get; set; }

    }
}
