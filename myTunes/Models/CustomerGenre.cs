﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class CustomerGenre
    {
        //Name of the genre
        public string Genre { get; set; }
        
        //Total invoices for the genre
        public int InvoicesForGenre { get; set; }

    }
}
