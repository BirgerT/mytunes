﻿namespace myTunes.Models
{
    public class SearchResult
    {
        public int TrackId { get; set; }
        public string TrackName { get; set; }
        public string ArtistName { get; set; }
        public string AlbumTitle { get; set; }
        public string Genre { get; set; }
    }
}
